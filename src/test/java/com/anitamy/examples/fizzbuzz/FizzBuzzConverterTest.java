package com.anitamy.examples.fizzbuzz;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class FizzBuzzConverterTest{
	private static final String FIZZ_BUZZ_OUPUT = " 1  2  fizz  4  buzz  fizz  7  8  fizz  buzz  11  fizz  13  14  fizzbuzz  16  17  fizz  19  buzz ";
	private final FizzBuzzConverterImpl fizzBuzz = new FizzBuzzConverterImpl();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	private int value;
	private String expected;

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
	}

	@After
	public void cleanUpStreams() {
		System.setOut(null);
	}

	public FizzBuzzConverterTest(final int value, String expected) {
		this.value = value;
		this.expected = expected;
	}

	@Parameters
	public static Iterable<Object[]> data() {
		return Arrays.asList(new Object[][] { { 1, "1" }, { 2, "2" }, { 3, "fizz" }, { 4, "4" }, { 5, "buzz" },
				{ 6, "fizz" }, { 7, "7" }, { 8, "8" }, { 9, "fizz" }, { 10, "buzz" }, { 11, "11" }, { 12, "fizz" },
				{ 13, "13" }, { 14, "14" }, { 15, "fizzbuzz" }, { 16, "16" }, { 17, "17" }, { 18, "fizz" },
				{ 19, "19" }, { 20, "buzz" } });
	}

	@Test(expected = IllegalArgumentException.class)
	public final void when_ZeroStartisUsed_ThrowsIllegalArgumentException() {
		fizzBuzz.convertNumbers(0, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void when_ZeroEndisUsed_ThrowsIllegalArgumentException() {
		fizzBuzz.convertNumbers(10, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void when_ZeroInvalidRanngeisUsed_ThrowsIllegalArgumentException() {
		fizzBuzz.convertNumbers(10, 5);
	}

	@Test
	public void when_NumbersFrom1to20Used_ReturnApropriateStringConversion() {
		assertEquals(expected, fizzBuzz.fizzBuzzMapper(value));
	}

	@Test
	public void when_NumbersFrom1to20Used_ReturnApropriateConvertedStringList() {
		assertEquals(getExpectedList(), fizzBuzz.convertNumbers(1, 20));
	}

	private List<String> getExpectedList() {
		final List<String> expectedList = new ArrayList<>();
		FizzBuzzConverterTest.data().forEach(obj -> expectedList.add(obj[1].toString()));
		return expectedList;
	}

	@Test(expected = IllegalArgumentException.class)
	public final void when_NullListisUsed_ThrowsIllegalArgumentException() {
		fizzBuzz.printValues(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void when_EmptyListisUsed_ThrowsIllegalArgumentException() {
		fizzBuzz.printValues(new ArrayList<>());
	}

	@Test
	public final void when_ListIsUsed_PrintsExpected() {
		fizzBuzz.printValues(getExpectedList());
		assertEquals(FIZZ_BUZZ_OUPUT, outContent.toString());
	}

	@Test
	public final void when_NumbersFrom1to20Used_PintsConvertedStringList() {
		fizzBuzz.convertAndPrintValues(1, 20);
		assertEquals(FIZZ_BUZZ_OUPUT, outContent.toString());
	}
}
