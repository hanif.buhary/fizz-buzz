package com.anitamy.examples.fizzbuzz;

/**
 * enum that represents number mapping
 * @author hanif
 */
public enum NumberMapping {
	FIZZ(3, "fizz"), BUZZ(5, "buzz");
	private int number;
	private String name;

	private NumberMapping(int number, String name) {
		this.number = number;
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}

}