/**
 * 
 */
package com.anitamy.examples.fizzbuzz;

/**
 * Converts continues range of numbers from 'start' to 'end' provided. Converts
 * as follows
 * <ul>
 * <li>'fizz' for number divisible by 3.
 * <li>'buzz' for number divisible by 5.
 * <li>'fizzbuzz' for number divisible by 15.
 * </ul>
 * 
 * @author hanif
 */

public interface FizzBuzzConverter {
	/**
	 * Accepts positive integer range and printout continues range of numbers
	 * from 'start' to 'end' provided. Exceptions are follows . 'fizz' for
	 * numbers that are multiples of 3 'buzz' for numbers that are multiples of
	 * 5 'fizzbuzz' for numbers that are multiples of 15
	 * 
	 * @param start:
	 *            a positive integer more than zero
	 * @param end:
	 *            a positive integer more than zero
	 * 
	 * @throws IllegalArgumentException
	 *             if either 'start' / 'end' is zero or negative if 'start' >
	 *             'end'
	 */
	public void convertAndPrintValues(final int start, final int end);
}
