package com.anitamy.examples.fizzbuzz;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

 /**
 * implementation of FizzBuzzConverter
 * 
 * @see FizzBuzzConverter
 * @author hanif
 */

public class FizzBuzzConverterImpl implements FizzBuzzConverter {

	private static final String EMPTY_STRING = "";
	private static final String SPACE_STR = " ";

	/**
	 * Checks whether rage is positive and
	 * starts from >0
	 */
	private final BiFunction<Integer, Integer, Boolean> invalidInputPredicate = (start, end) ->
	{
		return (start < 1 || end < 1 || end < start);
	};

	/**
	 * Represents a function that accepts an integer value and Number Mapping
	 * enum and returns name of enum. provided, 'number' property of the enum
	 * matches the given integer value. If the integer value doesn't match the
	 * 'number' property of the enum , it will return a blank string.
	 * 
	 * @param value:
	 *            Integer value to be converted.
	 * @param numberMapping:Enum
	 *            that needs to be matched.
	 * @return: If integer value matches the number returns name of the enum .If
	 *          not blank string.
	 */
	private final BiFunction<Integer, NumberMapping, String> convertNumber = (value, numberMapping) ->
	{
		return value % numberMapping.getNumber() == 0 ? numberMapping.getName() : EMPTY_STRING;
	};

	/**
	 * Represents a function that accepts an integer value and returns word
	 * 'fizz', 'buzz', 'fizzbuzz' or string representation of integer value.
	 * 
	 * @param value:Integer
	 *            value to be converted.
	 * @return: Returns 'fizz', 'buzz', 'fizzbuzz' , if value provided is
	 *          divisible by 3, 5 or 15 respectively. If value doesn't fall into
	 *          above categories it will return string representation of the
	 *          number.
	 */
	String fizzBuzzMapper(final int value) {
		final String convertedString = convertNumber.apply(value, NumberMapping.FIZZ)
				+ convertNumber.apply(value, NumberMapping.BUZZ);
		if (!convertedString.isEmpty()) {
			return convertedString;
		}
		return Integer.toString(value);
	};

	/**
	 * Accepts positive integer range and returns continues range of numbers
	 * from 'start' to 'end' provided. Exceptions are follows . 'fizz' for
	 * numbers that are multiples of 3 'buzz' for numbers that are multiples of
	 * 5 'fizzbuzz' for numbers that are multiples of 15
	 * 
	 * @param start:positive integer more than zero
	 * @param end:positive integer more than zero
	 * @throws IllegalArgumentException:if either 'start' / 'end' is zero or negative if 'start' > 'end'
	 * @return List of String representation of the numbers
	 */
	List<String> convertNumbers(final int start, final int end) {
		if (invalidInputPredicate.apply(start, end)) {
			throw new IllegalArgumentException("start and end should be >0 and start< end.");
		}
		return IntStream.rangeClosed(start, end).boxed().map(_number -> fizzBuzzMapper(_number))
				.collect(Collectors.toList());
	}

	/**
	 * Prints list of strings provided in a line with space separator
	 * 
	 * @param convertedStrings:list of string to be printed
	 * @throws IllegalArgumentException:if list is null of empty
	 */
	void printValues(List<String> convertedStrings) {
		if (!Optional.ofNullable(convertedStrings).isPresent() || convertedStrings.isEmpty()) {
			throw new IllegalArgumentException("List of string is empty !!");
		}
		convertedStrings.stream().map(str -> new StringBuilder(SPACE_STR).append(str).append(SPACE_STR).toString())
				.forEach(System.out::print);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void convertAndPrintValues(final int start, final int end) {
		printValues(convertNumbers(start, end));
	}

}
